package Game;

import java.awt.Color;
/*
 * Project Name:Cyvasse
 * Author: Angus McPherson
 * Version: v0.4
 * 
 * This is the main class for my project, it handles most of the game itself
 */
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

/*
 * Project Name:Cyvasse
 * Version number: a0.2
 * Author: Angus McPherson
 * This is the main class for the Cyvasse game, it handles all game play and calls the Frame class,
 * the Buttons class and the Piece class.
 */
public class FrameCyvasse {
	static String[][] TxtBoard = new String[8][8];
	static int[][] MoveBoard = new int[8][8];// A board to show what each pieces
												// moves are (only exists as the board has to remain static)
	static int[][] AtkBoard = new int [8][8];
	static boolean IsMove = true;
	static String CurrentPiece;
	static Piece[][] Pieces = new Piece[11][2];// An array for all the pieces
	static int q = 0;
	static BoardFrame Cyvasse = new BoardFrame("Cyvasse");
	public static void main(String[] args) {
		// creates a new frame
														// with the board built
														// into it
		Cyvasse.setTitle("Cyvasse");// sets title of window
		Cyvasse.setVisible(true);// sets the Window to be visible
	}

	public static void NewGame(Buttons[][] Board, JLabel NextPieceIMG, String FileName) {
		for (int x = 0; x < 4; x++) {
			for (int y = 0; y < 8; y++) {
				System.out.print(x + " " + y);
				Board[x][y].setEnabled(false);
				Board[x][y].enabled = false;// Sets all of the buttons in the
											// board to be usable
			}
		}
		ImageIcon NPiece = new ImageIcon(FileName + ".png");
		NextPieceIMG.setIcon(NPiece);// sets the nextPiece image to the upcoming
										// piece
		for (int a = 0; a <= 10; a++) {
			for (int j = 0; j < 2; j++) {
				if (j == 0) {
					Pieces[a][j] = new Piece(1, 1, (j + "" + a), 1, false);
				} else {
					Pieces[a][j] = new Piece(1, 1, (j + "" + a), 1, true);
				}
				if (a == 0) {
					Pieces[a][j].MoveDistance = 0;
					Pieces[a][j].NumOfPieces = 3;
				} else if (a == 4) {
					Pieces[a][j].MoveDistance = 3;
				} else if (a == 5) {
					Pieces[a][j].MoveDistance = 2;
				} else if (a == 9) {
					Pieces[a][j].MoveDistance = 20;
				} else if (a == 3) {
					Pieces[a][j].AttackDistance = 2;
				} else if (a == 8) {
					Pieces[a][j].AttackDistance = 3;
				} else if (a == 7) {
					Pieces[a][j].AttackDistance = 4;
				}
				// System.out.println(Pieces[a][j].FileName);
			}

		}

		BoardFrame.lblChooseWhereTo.setVisible(true);
		for (int x = 0; x < 8; x++) {
			for (int y = 0; y < 8; y++) {
				TxtBoard[x][y] = ("--");// sets the txtboard to have "--" as
										// each tiles value
				Board[x][y].moveHere = false;
			}
		}

		PlacePiece(Board, TxtBoard, Pieces[0][0], 0, 0, false);// Prompts the
																// user to place
																// a piece on
																// the board
	}

	static int moves = 0;


	static void BoardFix(Buttons[][] board) {
		for (int x = 0; x < 8; x++) {
			for (int y = 0; y < 8; y++) {
				if (TxtBoard[x][y] == "|") {
					board[x][y].addActionListener(null);
					board[x][y].setIcon(null);
					TxtBoard[x][y] = ("--");
				}
			}
		}
	}
	
	static ActionListener Attack;
	static int P1Score = 0;
	static int P2Score = 0;
	
	public static void Move(Buttons[][] board, int X, int Y, Piece PE, String[][] As) {
		
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				if (board[i][j].moveHere || board[i][j].getBackground()==Color.red) {
					MoveViewer(board, i, j, As);
					final int I = i;
					final int J = j;
					board[i][j].addActionListener(new ActionListener() {
						/*
						 * This Action listener is for moving the piece to its
						 * new location
						 */
						@Override
						public void actionPerformed(ActionEvent e) {
							if (CurrentPiece.equalsIgnoreCase(TxtBoard[X][Y])) {
								if (board[I][J].getBackground()==Color.red&&Player==1){
									P1Score+=1;
								}
								if (board[I][J].getBackground()==Color.red&&Player==0){
									P2Score+=1;
								}
								moves += 1;
								TxtBoard[I][J] = (TxtBoard[X][Y]);
								TxtBoard[X][Y] = ("--");
								board[I][J].P = board[X][Y].P;
								AtkBoard[I][J]=AtkBoard[X][Y];
								AtkBoard[X][Y] = 0;
								board[I][J].Player = board[X][Y].Player;
								board[X][Y].Player = 0;
								board[X][Y].removeActionListener(Test);
								for (int i = 0; i < 8; i++) {
									for (int j = 0; j < 8; j++) {
										board[i][j].removeActionListener(this);
										board[i][j].setBackground(board[i][j].Normal);
										board[I][J].setIcon(null);
										// board[I][J].P1 = board[X][Y].P1;
										MoveBoard[I][J] = MoveBoard[X][Y];
										
										// board[X][Y].P1 = null;
										board[X][Y].P = null;
										UpdateBoard(board);
									}
								}

								BoardFix(board);
								UpdateBoard(board);
								StartPlay(board, As);
							}
						}
					});
				}
			}
		}
	}
static int Player=0;
	public static void StartPlay(Buttons[][] board, String[][] txtBoard2) {
		if (Player == 1) {
			Player = 0;
			BoardFrame.AIvAI.setText("P=1");
		} else {
			Player = 1;
			BoardFrame.AIvAI.setText("P=2");
		}
		for (int x = 0; x < 8; x++) {
			for (int y = 0; y < 8; y++) {
				board[x][y].removeActionListener(Test);
				board[x][y].setEnabled(true);
				board[x][y].enabled = true;
				if (txtBoard2[x][y].equalsIgnoreCase("--")) {
					// no action Listener where there is no piece
				} else if (txtBoard2[x][y].equalsIgnoreCase("00")) {
					// Mn is a tile Piece that cannot move so i have not added
					// an action listener
				} else if (Player == board[x][y].Player) {
					MoveViewer(board, x, y, txtBoard2);
				}
			}
		}
	}

	static ActionListener Test;

	public static void MoveViewer(Buttons[][] board, int X, int Y, String[][] As) {
		UpdateBoard(board);
		if (Player == board[X][Y].Player) {
			ActionListener Move = new ActionListener() {

				public void actionPerformed(ActionEvent e) {// adds an
															// actionlistener
															// where there is a
															// piece
					// board[X][Y].removeActionListener(this);
					if (Player == board[X][Y].Player) {
						BoardFix(board);
						UpdateBoard(board);
						int Moves = MoveBoard[X][Y];
						Piece movingPiece = board[X][Y].P;
						for (int I = 0; I < 8; I++) {
							for (int J = 0; J < 8; J++) {
								board[I][J].setBackground(board[I][J].Normal);
								board[I][J].removeActionListener(this);// removes
																		// this
																		// actionlistener
								if (TxtBoard[I][J].equalsIgnoreCase("--")) {
									board[I][J].setIcon(null);
									board[I][J].moveHere = false;
									board[I][J].setBackground(board[I][J].Normal);
								}
							}
						}
						Boolean N = true, E = true, S = true, W = true;
						for (int i = 1; i <= Moves; i++) {
							if (Y + i < 8 && TxtBoard[X][Y + i].equalsIgnoreCase("--")
									&& (N || TxtBoard[X][Y].equalsIgnoreCase("09")
											|| TxtBoard[X][Y].equalsIgnoreCase("19"))) {// shows
																						// Moves
																						// to
																						// the
																						// right
								TxtBoard[X][Y + i] = "|";
								// board[X][Y].removeActionListener(this);
							}
							else if (Y + i < 8 && Player==board[X][Y+i].Player){
							}else{
								try {
									board[X][Y+i].setBackground(Color.RED);
								} catch (Exception e2) {
									// TODO: handle exception
								}
							}

							if (Y - i >= 0 && TxtBoard[X][Y - i].equalsIgnoreCase("--")
									&& (E || TxtBoard[X][Y].equalsIgnoreCase("09")
											|| TxtBoard[X][Y].equalsIgnoreCase("19"))) {// shows
																						// Moves
																						// to
																						// the
																						// left
								TxtBoard[X][Y - i] = "|";
								// board[X][Y].removeActionListener(this);
							} else if (Y - i >= 0 &&Player==board[X][Y-i].Player){
							}else{
								try {
									board[X][Y-i].setBackground(Color.RED);
								} catch (Exception e2) {
									// TODO: handle exception
								}
							}
							if (X - i >= 0 && TxtBoard[X - i][Y].equalsIgnoreCase("--")
									&& (S || TxtBoard[X][Y].equalsIgnoreCase("09")
											|| TxtBoard[X][Y].equalsIgnoreCase("19"))) {// shows
																						// Moves
																						// up
								TxtBoard[X - i][Y] = "|";
								// board[X][Y].removeActionListener(this);
							}
							else if (X - i >= 0 && Player==board[X-i][Y].Player){
								//Nothing
							}
							else{
								try {
									board[X-i][Y].setBackground(Color.RED);
								} catch (Exception e2) {
									// TODO: handle exception
								}
							}
							if (X + i < 8 && TxtBoard[X + i][Y].equalsIgnoreCase("--")
									&& (W || TxtBoard[X][Y].equalsIgnoreCase("09")
											|| TxtBoard[X][Y].equalsIgnoreCase("19"))) {// shows
																						// Moves
																						// down
								TxtBoard[X + i][Y] = "|";
								// board[X][Y].removeActionListener(this);
							} 
							else if (X + i < 8 && Player==board[X+i][Y].Player){
							} else{
								try {
									board[X+i][Y].setBackground(Color.RED);
								} catch (Exception e2) {
									// TODO: handle exception
								}
							}
							UpdateBoard(board);
							// basicGame.ViewBoard(TxtBoard);
						}
						CurrentPiece = TxtBoard[X][Y];
						// board[X][Y].removeActionListener(this);
						Move(board, X, Y, movingPiece, As);
					}
				}

			};
			Test = Move;
			board[X][Y].addActionListener(Test);
		}

	}

	int PlayerTurn;

	public static void UpdateBoard(Buttons[][] board) {
		/*
		 * For updating the board, this method is called whenever a change is
		 * made to the board
		 */
		// basicGame.ViewBoard(TxtBoard);
		if (P1Score>=10){
			for (int x = 0; x < 8; x++) {
				for (int y = 0; y < 8; y++) {
					board[x][y].setEnabled(false);
					Cyvasse.lblChooseWhereTo.setText("Player 1 Wins!!!");
				}
			}
		}
		if (P2Score>=10){
			for (int x = 0; x < 8; x++) {
				for (int y = 0; y < 8; y++) {
					board[x][y].setEnabled(false);
					Cyvasse.lblChooseWhereTo.setText("Player 2 Wins!!!");
				}
			}
		}
		for (int x = 0; x < 8; x++) {
			for (int y = 0; y < 8; y++) {
				//board[x][y].setBackground(board[x][y].Normal);
				if (TxtBoard[x][y].equalsIgnoreCase("--")) {
					board[x][y].setIcon(null);
					board[x][y].P = new Piece(0, 0, "--", 0, true);
					board[x][y].moveHere = false;
					board[x][y].setEnabled(false);
				} else if (TxtBoard[x][y].equalsIgnoreCase("|")) {
					ImageIcon Target = new ImageIcon("Target.png");
					board[x][y].setIcon(Target);
					board[x][y].moveHere = true;
					board[x][y].setEnabled(true);
				} else {
					ImageIcon Piece = new ImageIcon(TxtBoard[x][y] + ".png");
					board[x][y].setIcon(Piece);
					board[x][y].moveHere = false;
					board[x][y].setEnabled(true);
				}
			}
		}
	}

	public static int PieceLeft = 0;

	public static void PlacePiece(Buttons[][] board, String[][] txtBoard2, Piece PC, int i, int j, boolean Play) {
		/*
		 * This section is used at the start of the game to allow the players to
		 * place their pieces on their side of the board, it also sets all of
		 * the relevant variables for each piece on the button it does so by
		 * cycling through the file names of each piece using a for loop, The
		 * first half being changed from 0 to 1 once the second variable has
		 * reached 10(The last Piece)
		 */
		PieceLeft = PC.NumOfPieces;
		ImageIcon NPiece = new ImageIcon(PC.FileName + ".png");
		BoardFrame.NextPieceIMG.setIcon(NPiece);
		for (int x = 0; x < 8; x++) {
			for (int y = 0; y < 8; y++) {
				final int X = x;
				final int Y = y;
				ActionListener Butts = new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						if (txtBoard2[X][Y].equals("--") && PieceLeft > 0) {
							board[X][Y].P = PC;
							board[X][Y].Moves = PC.MoveDistance;
							MoveBoard[X][Y] = PC.MoveDistance;
							AtkBoard[X][Y]=PC.AttackDistance;
							board[X][Y].SetIcon(NPiece);
							board[X][Y].P.FileName = PC.FileName;
							board[X][Y].P.Player = Play;
							Buttons.Piece = PC.FileName;
							txtBoard2[X][Y] = PC.FileName;
							board[X][Y].X = X;
							board[X][Y].Y = Y;
							board[X][Y].Player = i;
							PieceLeft = PieceLeft - 1;
							int J2 = j + 1;
							int I2 = i;
							if (j == 10 && i == 0) {
								for (int x = 0; x < 8; x++) {
									for (int y = 0; y < 8; y++) {
										board[x][y].setEnabled(true);// sets
																		// half
																		// of
																		// the
																		// board
																		// to be
																		// enabled
																		// to
																		// allow
																		// other
																		// player
																		// to
																		// place
																		// their
																		// pieces
									}
								}
								for (int x = 4; x < 8; x++) {
									for (int y = 0; y < 8; y++) {
										board[x][y].setEnabled(false);// sets
																		// the
																		// other
																		// half
																		// to be
																		// diabled
									}
								}
								J2 = 0;// changes the second variable(Piece
										// number) to 0, which is the firstr
										// piece
								I2 = 1;// changes the first variable(player) to
										// 1, which changes from the first
										// players pieces to the second player's
							} else {
								J2 = j + 1;// just increases the second variable
											// by 1 moving onto the next piece
							}
							if (j == 10 && i == 1) {
								for (int x = 0; x < 8; x++) {
									for (int y = 0; y < 8; y++) {
										board[x][y].removeActionListener(this);// removes
																				// the
																				// actionlistener
																				// from
																				// all
																				// tiles
									}
								}
								StartPlay(board, TxtBoard);// once the
																	// last
																	// piece has
																	// been
																	// placed it
																	// starts
																	// the game
								BoardFrame.NextPieceIMG.setIcon(null);// removes
																		// the
																		// icon
																		// for
																		// next
																		// image
							}
							if (PieceLeft <= 0 && J2 < 11) {
								PlacePiece(board, TxtBoard, Pieces[J2][I2], I2, J2, true);
							}
							board[X][Y].removeActionListener(this);
						}
					}

				};

				board[x][y].addActionListener(Butts);
			}
		}
	}

}
