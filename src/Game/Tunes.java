package Game;

import java.io.File;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

/*
 * A way to start playing music inside the program this will not be in the final version
 * 
 */


public class Tunes {
	public static void Tunes(String FilePath){
		try {
	        AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(FilePath).getAbsoluteFile());
	        Clip clip = AudioSystem.getClip();
	        clip.open(audioInputStream);
	        clip.start();
	    } catch(Exception ex) {
	        System.out.println("Error with playing the Tunes.");
	        ex.printStackTrace();
	    }
	}

}
