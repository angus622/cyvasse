package Game;

import java.util.Scanner;
import javax.swing.JPanel;

public class basicGame extends JPanel{
	
	public static void main(String[] args) {
		
		String[][] Board = new String[8][8];// The Array for the Board
		// VBF(Board);
		Menu(Board);
		// BoardSetup(Board);
		// ViewBoard(Board);

	}

	static void Menu(String[][] Board) {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		System.out
				.println("Hello and welcome To Cyvasse!\n\nWhat would you like to do?\n1)New Game\n2)Settings\n3)Exit");
		int MenuChoice = sc.nextInt();
		if (MenuChoice == 1) {
			BoardSetup(Board);
			System.out.println("Select Mode:\n\n1)Player v Player\n2)Player v AI\n3)AI v AI");
			MenuChoice = sc.nextInt();
			if (MenuChoice == 1) {
				UserBoard(Board);
			} else {
				Nyi(Board);// Message Saying a feature is not yet implemented
							// that brings user back to Main menu
			}
		} else if (MenuChoice == 2) {
			Nyi(Board);
		} else {
			System.exit(0);
		}
	}

	static void UserBoard(String[][] Board) {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		String[][] BoardP1 = new String[8][8];// secondary board for player one
												// to place units
		BoardSetup(BoardP1);
		for (int x = 0; x < 8; x++) {
			for (int y = 0; y < 4; y++) {
				BoardP1[y][x] = ("X");
			}
		}
		int[] RabbleCoords = new int[8];
		int[] SpearCoords = new int[8];
		int[] CrossCoords = new int[8];
		int[] LightCoords = new int[8];
		int[] heavyCoords = new int[8];
		int[] EleCoords = new int[8];
		int[] catCoords = new int[8];
		int[] trebCoords = new int[8];
		int[] kingCoords = new int[8];
		int[] dragCoords = new int[8];

		PlaceUnit(Board, 1, "Rabble", "R", BoardP1, RabbleCoords);// Calls PlaceUnit to place unit
		PlaceUnit(Board, 1, "Spearmen", "S", BoardP1, SpearCoords);// Calls PlaceUnit to place unit
		PlaceUnit(Board, 1, "Crossbow", "B", BoardP1, CrossCoords);// Calls PlaceUnit to place unit
		PlaceUnit(Board, 1, "Light Horse", "L", BoardP1, LightCoords);// Calls PlaceUnit to place unit
		PlaceUnit(Board, 1, "Heavy Horse", "H", BoardP1, heavyCoords);// Calls PlaceUnit to place unit
		PlaceUnit(Board, 1, "Elephant", "E", BoardP1, EleCoords);// Calls PlaceUnit to place unit
		PlaceUnit(Board, 1, "Catapult", "C", BoardP1, catCoords);// Calls PlaceUnit to place unit
		PlaceUnit(Board, 1, "Trebuchet", "T", BoardP1, trebCoords);// Calls PlaceUnit to place unit
		PlaceUnit(Board, 1, "King", "K", BoardP1, kingCoords);// Calls PlaceUnit to place unit
		PlaceUnit(Board, 1, "Dragon", "D", BoardP1, dragCoords);// Calls PlaceUnit to place unit

		String[][] BoardP2 = new String[8][8];// secondary board for player two
												// to place units
		BoardSetup(BoardP2);
		for (int x = 0; x < 8; x++) {
			for (int y = 4; y < 8; y++) {
				BoardP2[y][x] = ("X");
			}
		}

		int[] RabbleCoords1 = new int[8];
		int[] SpearCoords1 = new int[8];
		int[] CrossCoords1 = new int[8];
		int[] LightCoords1 = new int[8];
		int[] heavyCoords1 = new int[8];
		int[] EleCoords1 = new int[8];
		int[] catCoords1 = new int[8];
		int[] trebCoords1 = new int[8];
		int[] kingCoords1 = new int[8];
		int[] dragCoords1 = new int[8];

		PlaceUnit(Board, 1, "Rabble", "r", BoardP2, RabbleCoords1);// Calls PlaceUnit to place unit
		PlaceUnit(Board, 1, "Spearmen", "d", BoardP2, SpearCoords1);// Calls PlaceUnit to place unit
		PlaceUnit(Board, 1, "Crossbow", "b", BoardP2, CrossCoords1);// Calls PlaceUnit to place unit
		PlaceUnit(Board, 1, "Light Horse", "l", BoardP2, LightCoords1);// Calls PlaceUnit to place unit
		PlaceUnit(Board, 1, "Heavy Horse", "h", BoardP2, heavyCoords1);// Calls PlaceUnit to place unit
		PlaceUnit(Board, 1, "Elephant", "e", BoardP2, EleCoords1);// Calls PlaceUnit to place unit
		PlaceUnit(Board, 1, "Catapult", "c", BoardP2, catCoords1);// Calls PlaceUnit to place unit
		PlaceUnit(Board, 1, "Trebuchet", "t", BoardP2, trebCoords1);// Calls PlaceUnit to place unit
		PlaceUnit(Board, 1, "King", "k", BoardP2, kingCoords1);// Calls PlaceUnit to place unit
		PlaceUnit(Board, 1, "Dragon", "d", BoardP2, dragCoords1);// Calls PlaceUnit to place unit
		
		boolean CanRabble1 = true;
		boolean CanSpear1 = true;
		boolean CanBow1 = true;
		boolean CanLight1 = true;
		boolean CanHeavy1 = true;
		boolean CanEle1 = true;
		boolean CanCat1 = true;
		boolean Cantreb1 = true;
		boolean CanKing1 = true;
		boolean CanDragon1 =true;
		
		boolean aliveRabble1 = true;
		boolean aliveSpear1 = true;
		boolean aliveBow1 = true;
		boolean aliveLight1 = true;
		boolean aliveHeavy1 = true;
		boolean aliveEle1 = true;
		boolean aliveCat1 = true;
		boolean alivetreb1 = true;
		boolean aliveKing1 = true;
		boolean aliveDragon1 =true;
		
		boolean aliveRabble2 = true;
		boolean aliveSpear2 = true;
		boolean aliveBow2 = true;
		boolean aliveHeavy2 = true;
		boolean aliveEle2 = true;
		boolean aliveCat2 = true;
		boolean alivetreb2 = true;
		boolean aliveKing2 = true;
		boolean aliveDragon2 =true;
		String Winner= "null";
		int fin = 1, XCo = 1, YCo = 1, Player = 1;
		while (fin == 1) {
			int Moves = 3;
			 CanRabble1 = true;
			 CanSpear1 = true;
			 CanBow1 = true;
			 CanLight1 = true;
			 CanHeavy1 = true;
			 CanEle1 = true;
			 CanCat1 = true;
			 Cantreb1 = true;
			 CanKing1 = true;
			 CanDragon1 =true;
			
			 String Dead="n";
			String[][] moveableSpaces = Board;
			if (Player == 1) {
				Player = 2;
				while (Moves > 0) {
					BoardFix(Board);
					ViewBoard(Board);
					System.out.println("Player 1 please select the unit you want to move(" + Moves + " moves left): ");
					String SelectedPiece = sc.nextLine();
					if (SelectedPiece.equalsIgnoreCase("Rabble") || SelectedPiece.equalsIgnoreCase("r")&&aliveRabble1&&CanRabble1) {
						MoveUnit(Board, moveableSpaces, RabbleCoords, XCo, YCo, "R", 1, Moves,aliveRabble1,Dead);
						Moves -= 1;
						CanRabble1=false;
					} else if (SelectedPiece.equalsIgnoreCase("Spearman") || SelectedPiece.equalsIgnoreCase("s")&&aliveSpear1&&CanSpear1) {
						MoveUnit(Board, moveableSpaces, SpearCoords, XCo, YCo, "S", 1, Moves,aliveSpear1,Dead);
						Moves -= 1;
						CanSpear1=false;
					} else if (SelectedPiece.equalsIgnoreCase("CrossBow") || SelectedPiece.equalsIgnoreCase("b")&&CanBow1&&aliveBow1) {
						MoveUnit(Board, moveableSpaces, CrossCoords, XCo, YCo, "B", 1, Moves,aliveBow1,Dead);
						Moves -= 1;
						CanBow1=false;
					} else if (SelectedPiece.equalsIgnoreCase("Light Horse") || SelectedPiece.equalsIgnoreCase("l")&&CanLight1&&aliveLight1) {
						MoveUnit(Board, moveableSpaces, LightCoords, XCo, YCo, "L", 3, Moves,aliveLight1,Dead);
						Moves -= 1;
						CanLight1=false;
					} else if (SelectedPiece.equalsIgnoreCase("Heavy Horse") || SelectedPiece.equalsIgnoreCase("h")&&CanHeavy1&&aliveHeavy1) {
						MoveUnit(Board, moveableSpaces, heavyCoords, XCo, YCo, "H", 2, Moves,aliveHeavy1,Dead);
						Moves -= 1;
						CanHeavy1=false;
					} else if (SelectedPiece.equalsIgnoreCase("Elephant") || SelectedPiece.equalsIgnoreCase("e")&&CanEle1&&aliveEle1) {
						MoveUnit(Board, moveableSpaces, EleCoords, XCo, YCo, "E", 1, Moves,aliveEle1,Dead);
						Moves -= 1;
						CanEle1=false;
					} else if (SelectedPiece.equalsIgnoreCase("Trebuchet") || SelectedPiece.equalsIgnoreCase("t")&&Cantreb1&&alivetreb1) {
						MoveUnit(Board, moveableSpaces, trebCoords, XCo, YCo, "T", 1, Moves,alivetreb1,Dead);
						Moves -= 1;
						Cantreb1=false;
					} else if (SelectedPiece.equalsIgnoreCase("catapult") || SelectedPiece.equalsIgnoreCase("c")&&CanCat1&&aliveCat1) {
						MoveUnit(Board, moveableSpaces, catCoords, XCo, YCo, "C", 1, Moves,aliveCat1,Dead);
						Moves -= 1;
						CanCat1=false;
					} else if (SelectedPiece.equalsIgnoreCase("Dragon") || SelectedPiece.equalsIgnoreCase("d")&&CanDragon1&&aliveDragon1) {
						MoveUnit(Board, moveableSpaces, dragCoords, XCo, YCo, "D", 20, Moves,aliveDragon1,Dead);
						Moves -= 1;
						CanDragon1=false;
					} else if (SelectedPiece.equalsIgnoreCase("King") || SelectedPiece.equalsIgnoreCase("k")&&CanKing1&&aliveKing1) {
						MoveUnit(Board, moveableSpaces, kingCoords, XCo, YCo, "K", 1, Moves,aliveKing1,Dead);
						Moves -= 1;
						CanKing1=false;
					}
					System.out.print(Dead);
					if (Dead.equalsIgnoreCase("r")){
						aliveRabble1=false;
						Dead="n";
					}else if (Dead.equalsIgnoreCase("s")){
						aliveSpear1=false;
						Dead="n";
					}else if (Dead.equalsIgnoreCase("b")){
						aliveBow1=false;
						Dead="n";
					}else if (Dead.equalsIgnoreCase("l")){
						aliveLight1=false;
						Dead="n";
					}else if (Dead.equalsIgnoreCase("h")){
						aliveHeavy1=false;
						Dead="n";
					}else if (Dead.equalsIgnoreCase("e")){
						aliveEle1=false;
						Dead="n";
					}else if (Dead.equalsIgnoreCase("t")){
						alivetreb1=false;
						Dead="n";
					}else if (Dead.equalsIgnoreCase("c")){
						aliveCat1=false;
						Dead="n";
					}else if (Dead.equalsIgnoreCase("d")){
						aliveDragon1=false;
						Dead="n";
					}else if (Dead.equalsIgnoreCase("k")){
						aliveKing1=false;
						Dead="n";
						fin=2;
						Winner="Player 1";
						Menu(Board);
					}
				}
			}else if (Player == 2) {

				Player = 1;
				while (Moves > 0) {
					BoardFix(Board);
					ViewBoard(Board);
					System.out.println("Player 2 please select the unit you want to move(" + Moves + " moves left): ");
					String SelectedPiece = sc.nextLine();
					if (SelectedPiece.equalsIgnoreCase("Rabble") || SelectedPiece.equalsIgnoreCase("r")&&aliveRabble1&&CanRabble1) {
						MoveUnit(Board, moveableSpaces, RabbleCoords1, XCo, YCo, "r", 1, Moves,aliveRabble2,Dead);
						Moves -= 1;
						CanRabble1=false;
					} else if (SelectedPiece.equalsIgnoreCase("Spearman") || SelectedPiece.equalsIgnoreCase("s")&&aliveSpear1&&CanSpear1) {
						MoveUnit(Board, moveableSpaces, SpearCoords1, XCo, YCo, "s", 1, Moves,aliveSpear2,Dead);
						Moves -= 1;
						CanSpear1=false;
					} else if (SelectedPiece.equalsIgnoreCase("CrossBow") || SelectedPiece.equalsIgnoreCase("b")&&CanBow1&&aliveBow1) {
						MoveUnit(Board, moveableSpaces, CrossCoords1, XCo, YCo, "b", 1, Moves,aliveBow2,Dead);
						Moves -= 1;
						CanBow1=false;
					} else if (SelectedPiece.equalsIgnoreCase("Light Horse") || SelectedPiece.equalsIgnoreCase("l")&&CanLight1&&aliveLight1) {
						MoveUnit(Board, moveableSpaces, LightCoords1, XCo, YCo, "l", 3, Moves,aliveLight1,Dead);
						Moves -= 1;
						CanLight1=false;
						System.out.println(Dead);
					} else if (SelectedPiece.equalsIgnoreCase("Heavy Horse") || SelectedPiece.equalsIgnoreCase("h")&&CanHeavy1&&aliveHeavy1) {
						MoveUnit(Board, moveableSpaces, heavyCoords1, XCo, YCo, "h", 2, Moves,aliveHeavy2,Dead);
						Moves -= 1;
						CanHeavy1=false;
					} else if (SelectedPiece.equalsIgnoreCase("Elephant") || SelectedPiece.equalsIgnoreCase("e")&&CanEle1&&aliveEle1) {
						MoveUnit(Board, moveableSpaces, EleCoords1, XCo, YCo, "e", 1, Moves,aliveEle2,Dead);
						Moves -= 1;
						CanEle1=false;
					} else if (SelectedPiece.equalsIgnoreCase("Trebuchet") || SelectedPiece.equalsIgnoreCase("t")&&Cantreb1&&alivetreb1) {
						MoveUnit(Board, moveableSpaces, trebCoords1, XCo, YCo, "t", 1, Moves,alivetreb2,Dead);
						Moves -= 1;
					} else if (SelectedPiece.equalsIgnoreCase("catapult") || SelectedPiece.equalsIgnoreCase("c")&&CanCat1&&aliveCat1) {
						MoveUnit(Board, moveableSpaces, catCoords1, XCo, YCo, "c", 1, Moves,aliveCat2,Dead);
						Moves -= 1;
					} else if (SelectedPiece.equalsIgnoreCase("Dragon") || SelectedPiece.equalsIgnoreCase("d")&&CanDragon1&&aliveDragon1) {
						MoveUnit(Board, moveableSpaces, dragCoords1, XCo, YCo, "d", 20, Moves,aliveDragon2,Dead);
						Moves -= 1;
					} else if (SelectedPiece.equalsIgnoreCase("King") || SelectedPiece.equalsIgnoreCase("k")&&CanKing1&&aliveKing1) {
						MoveUnit(Board, moveableSpaces, kingCoords1, XCo, YCo, "k", 1, Moves,aliveKing2,Dead);
						Moves -= 1;
					}
					System.out.println(Dead);
					if (Dead.equalsIgnoreCase("R")){
						aliveRabble1=false;
					}else if (Dead.equalsIgnoreCase("S")){
						aliveSpear1=false;
					}else if (Dead.equalsIgnoreCase("B")){
						aliveBow1=false;
					}else if (Dead.equalsIgnoreCase("L")){
						aliveLight1=false;
					}else if (Dead.equalsIgnoreCase("H")){
						aliveHeavy1=false;
					}else if (Dead.equalsIgnoreCase("E")){
						aliveEle1=false;
					}else if (Dead.equalsIgnoreCase("T")){
						alivetreb1=false;
					}else if (Dead.equalsIgnoreCase("C")){
						aliveCat1=false;
					}else if (Dead.equalsIgnoreCase("D")){
						aliveDragon1=false;
					}else if (Dead.equalsIgnoreCase("K")){
						aliveKing1=false;
						fin=2;
						Winner="Player 2";
						Menu(Board);
					}
				}
			}
		}
		System.out.println("The Winner is "+Winner);
	}

	static void MoveUnit(String[][] Board, String[][] moveableSpaces, int[] Coords, int XCo, int YCo, String name,
			int Moves, int moves,boolean dead1,String dead ) {
		dead="k";
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		UnitMoves(moveableSpaces, Moves, Coords);
		System.out.println("The Places You can move have been Highlighted with the char\"|\",\n input X coordinate");
		XCo = sc.nextInt();
		XCo = XCo - 1;
		System.out.println("intput Y coordinate");
		YCo = sc.nextInt();
		YCo = YCo - 1;
		boolean canAttack = true;
		if(YCo<9&&XCo<9&&YCo>0&&XCo>0){
				if (moveableSpaces[YCo][XCo] == "|") {
					Board[Coords[2]][Coords[1]] = "-";
					Board[YCo][XCo] = name;
				}else if (canAttack&&Board[YCo][XCo] == "r"||Board[YCo][XCo] == "s"||Board[YCo][XCo] == "b"||Board[YCo][XCo] == "l"||Board[YCo][XCo] == "h"||Board[YCo][XCo] == "e"||Board[YCo][XCo] == "c"||Board[YCo][XCo] == "t"||Board[YCo][XCo] == "k"||Board[YCo][XCo] == "d"){
					int AttackA = AttackValue(name);
					int AttackB = AttackValue(Board[YCo][XCo] );
					int DefendA = HealthValue(name);
					int DefendB = HealthValue(Board[YCo][XCo] );
					if (AttackA>=DefendB){
						System.out.println(Board[YCo][XCo]);
						dead=Board[YCo][XCo];
						System.out.println(dead);
						Board[Coords[2]][Coords[1]] = "-";
						Board[YCo][XCo] = name;
						if (dead.equalsIgnoreCase("k")){
							System.out.println("The Winner is player 1");
							System.exit(0);
						}
					}else if (DefendA<(AttackB-1)){
						dead1=true;
						Board[Coords[2]][Coords[1]] = "-";
					}else if (AttackA==(4-1)){
						dead1=true;
						dead=Board[YCo][XCo];
						Board[Coords[2]][Coords[1]] = "-";
						Board[YCo][XCo] = "-";
						if (dead.equalsIgnoreCase("k")){
							System.out.println("The Winner is player 1");
							System.exit(0);
						}
					}
					else{
						dead="N";
					}

				}
				else {
					System.out.println("Cant Move here");
					Moves+=1;
				}
		}
	}
	
	static void MoveUnitB(String[][] Board, String[][] moveableSpaces, int[] Coords, int XCo, int YCo, String name,
			int Moves, int moves,boolean dead1,String dead ) {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		UnitMoves(moveableSpaces, Moves, Coords);
		System.out.println("The Places You can move have been Highlighted with the char\"|\",\n input X coordinate");
		XCo = sc.nextInt();
		XCo = XCo - 1;
		System.out.println("intput Y coordinate");
		YCo = sc.nextInt();
		YCo = YCo - 1;
		boolean canAttack = true;
		if(YCo<9&&XCo<9&&YCo>0&&XCo>0){
				if (moveableSpaces[YCo][XCo] == "|") {
					Board[Coords[2]][Coords[1]] = "-";
					Board[YCo][XCo] = name;
					Coords[1]=XCo;
					Coords[1]=YCo;
				}else if (canAttack&&Board[YCo][XCo] == "R"||Board[YCo][XCo] == "S"||Board[YCo][XCo] == "B"||Board[YCo][XCo] == "L"||Board[YCo][XCo] == "H"||Board[YCo][XCo] == "eE"||Board[YCo][XCo] == "C"||Board[YCo][XCo] == "T"||Board[YCo][XCo] == "K"||Board[YCo][XCo] == "D"){
					int AttackA = AttackValue(name);
					int AttackB = AttackValue(Board[YCo][XCo] );
					int DefendA = HealthValue(name);
					int DefendB = HealthValue(Board[YCo][XCo] );
					if (AttackA>=DefendB){
						dead=Board[YCo][XCo];
						Board[Coords[2]][Coords[1]] = "-";
						Board[YCo][XCo] = name;
						Coords[1]=XCo;
						Coords[1]=YCo;
						if (dead.equalsIgnoreCase("k")){
							System.out.println("The Winner is player 1");
							System.exit(0);
						}
					}else if (DefendA<(AttackB-1)){
						dead1=true;
						Board[Coords[2]][Coords[1]] = "-";
						Coords[1]=XCo;
						Coords[1]=YCo;
					}else if (AttackA==(4-1)){
						dead1=true;
						dead=Board[YCo][XCo];
						Board[Coords[2]][Coords[1]] = "-";
						Board[YCo][XCo] = "-";
						Coords[1]=XCo;
						Coords[1]=YCo;
						if (dead.equalsIgnoreCase("k")){
							System.out.println("The Winner is player 1");
							System.exit(0);
						}
					}
					
				}
				else {
					System.out.println("Cant Move here");
					Moves+=1;
				}
		}
	}
		
	static int AttackValue(String Name){
		if (Name.equalsIgnoreCase("r")){
			return(1);
		}
		else if (Name.equalsIgnoreCase("s")){
			return(2);
		}
		else if (Name.equalsIgnoreCase("b")){
			return(2);
		}
		else if (Name.equalsIgnoreCase("l")){
			return(3);
		}
		else if (Name.equalsIgnoreCase("h")){
			return(4);
		}
		else if (Name.equalsIgnoreCase("e")){
			return(4);
		}
		else if (Name.equalsIgnoreCase("c")){
			return(3);
		}
		else if (Name.equalsIgnoreCase("t")){
			return(4);
		}
		else if (Name.equalsIgnoreCase("d")){
			return(5);
		}
		else if (Name.equalsIgnoreCase("k")){
			return(5);
		}
		else{
		return 0;
		}
	}
	
	static int HealthValue(String Name){
		if (Name.equalsIgnoreCase("r")){
			return(1);
		}
		else if (Name.equalsIgnoreCase("s")){
			return(2);
		}
		else if (Name.equalsIgnoreCase("b")){
			return(2);
		}
		else if (Name.equalsIgnoreCase("l")){
			return(3);
		}
		else if (Name.equalsIgnoreCase("h")){
			return(4);
		}
		else if (Name.equalsIgnoreCase("e")){
			return(4);
		}
		else if (Name.equalsIgnoreCase("c")){
			return(1);
		}
		else if (Name.equalsIgnoreCase("t")){
			return(1);
		}
		else if (Name.equalsIgnoreCase("d")){
			return(5);
		}
		else if (Name.equalsIgnoreCase("k")){
			return(1);
		}
		return 0;
		
	}
	

	static void UnitMoves(String[][] Moves, int moves, int[] Coords) {
		for (int i = 0; i <= moves; i++) {

			if (Coords[2] + i < 8 && Moves[Coords[2] + i][Coords[1]] == "-") {
				Moves[Coords[2] + i][Coords[1]] = "|";
			}
			if (Coords[2] + i < 8 && Coords[1] + i < 8 && Moves[Coords[2] + i][Coords[1] + i] == "-") {
				Moves[Coords[2] + i][Coords[1] + i] = "|";
			}
			if (Coords[1] + i < 8 && Moves[Coords[2]][Coords[1] + i] == "-") {
				Moves[Coords[2]][Coords[1] + i] = "|";
			}
			if (Coords[1] - i > -1 && Coords[2] - i > -1 && Moves[Coords[2] - i][Coords[1] - i] == "-") {
				Moves[Coords[2] - i][Coords[1] - i] = "|";
			}
			if (Coords[1] - i > -1 && Moves[Coords[2]][Coords[1] - i] == "-") {
				Moves[Coords[2]][Coords[1] - i] = "|";
			}
			if (Coords[2] - i > -1 && Moves[Coords[2] - i][Coords[1]] == "-") {
				Moves[Coords[2] - i][Coords[1]] = "|";
			}
			if (Coords[1] + i < 8 && Coords[2] - i > -1 && Moves[Coords[2] - i][Coords[1] + i] == "-") {
				Moves[Coords[2] - i][Coords[1] + i] = "|";
			}
			if (Coords[1] - i > -1 && Coords[2] + i < 8 && Moves[Coords[2] + i][Coords[1] - i] == "-") {
				Moves[Coords[2] + i][Coords[1] - i] = "|";
			}
		}
		ViewBoard(Moves);
	}

	static void PlaceUnit(String[][] Board, int PieceNum, String UnitName, String UnitChar, String[][] PlayerBoard,
			int[] coordsF) {
		ViewBoard(PlayerBoard);
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		int PiecesLeft = PieceNum;
		do {
			System.out.print("\nPlease select X Co-ord of " + UnitName + " number " + PiecesLeft);
			int XCoord = sc.nextInt();
			XCoord = XCoord - 1;
			System.out.print("\nPlease select Y Co-ord of " + UnitName + " number " + PiecesLeft);
			int YCoord = sc.nextInt();
			YCoord = YCoord - 1;
			if (YCoord > 7 || XCoord >= 8 || XCoord < 0 || PlayerBoard[YCoord][XCoord] == "X") {
				System.out.println("Coordinate Out of Range please try again");
			} else if (Board[YCoord][XCoord].equals("-")) {
				Board[YCoord][XCoord] = UnitChar;
				PlayerBoard[YCoord][XCoord] = UnitChar;
				coordsF[1] = XCoord;
				coordsF[2] = YCoord;
				PiecesLeft -= 1;
			} else {
				System.out.println("There is already a piece here please try again");
			}
			ViewBoard(PlayerBoard);
		} while (PiecesLeft > 0);

	}

	static void Nyi(String[][] Board) {
		// Message Saying a feature is not yet implemented that brings user back
		// to main menu
		System.out.println("Not Yet Implemented");
		Menu(Board);
	}

	static void BoardFix(String[][] Board) {
		for (int x = 0; x < 8; x++) {
			for (int y = 0; y < 8; y++) {
				if (Board[x][y] == "|") {
					Board[x][y] = ("-");// -= Grass Tiles; which are the basic
										// tiles
										// that cover the board pre user input
				}
			}
		}
	}

	static void BoardSetup(String[][] Board) {
		for (int x = 0; x < 8; x++) {
			for (int y = 0; y < 8; y++) {
				Board[x][y] = ("-");// -= Grass Tiles; which are the basic tiles
									// that cover the board pre user input
			}
		}
	}


	static void ViewBoard(String[][] Board) {
		System.out.print("0  1  2  3  4  5  6  7  8");
		for (int x = 0; x < 8; x++) {
			System.out.print("\n" + (x + 1) + " ");
			for (int y = 0; y < 8; y++) {
				if (Board[x][y].equalsIgnoreCase("110")||Board[x][y].equalsIgnoreCase("010")){
					System.out.print("KI ");
				}else{
				System.out.print(Board[x][y] + " ");
				}
			}

		}
		System.out.print("\n\n");
	}
	static void ViewBoardi(int[][] Board) {
		System.out.print("0 1 2 3 4 5 6 7 8");
		for (int x = 0; x < 8; x++) {
			System.out.print("\n" + (x + 1) + " ");
			for (int y = 0; y < 8; y++) {
				if (x==1023){
					System.out.print("KI ");
				}else{
				System.out.print(Board[x][y] + " ");
				}
			}

		}
		System.out.print("\n\n");
	}
	
}
