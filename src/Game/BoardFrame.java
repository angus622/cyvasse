package Game;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.activation.ActivationID;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import java.awt.Cursor;
import java.awt.Component;
import javax.swing.border.BevelBorder;
import javax.swing.SwingConstants;

public class BoardFrame extends JFrame {

	JPanel panel = new JPanel(new GridLayout(8, 8, 0, 0));
	static JButton Cancel = new JButton("Cancel");
	static JButton AIvAI = new JButton("P=1");
	static JButton P1vP2 = new JButton("P1 v P2");
	static JButton P1vAI = new JButton("Music");
	static JLabel lblChooseWhereTo = new JLabel("Choose where to place the piece");
	Color Sand = new Color(158, 146, 99);
	Color DarkSand = new Color(82, 77, 59);
	JLayeredPane panel2 = new JLayeredPane();
	JPanel InfoPane = new JPanel();
	JButton NewGame = new JButton();
	JButton Exit = new JButton();
	JLabel NextPiece = new JLabel("Upcoming Piece: ");
	JButton SecretTunes = new JButton();
	static JLabel NextPieceIMG = new JLabel();
	ImageIcon img = new ImageIcon("Cyvasse.png");
	static Buttons[][] Board2 = new Buttons[8][8];
	JTextArea Instructions = new JTextArea();

	public BoardFrame(String WindowName) {
		setTitle("Cyvasse!");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		int PieceNum = 0;
		Boolean Odd = false;
		Boolean OddRow = false;
		for (int x = 0; x < 8; x++) {
			Odd = !Odd;
			for (int y = 0; y < 8; y++) {
				OddRow=!OddRow;
				if(Odd&&!OddRow){
					PieceNum=1;
				}
				else if(!Odd&&OddRow){
					PieceNum=1;
				}
				else if(!Odd&&!OddRow){
					PieceNum=2;
				}
				else if(Odd&&OddRow){
					PieceNum=2;
				}
				Board2[x][y] = new Buttons("Grass", PieceNum, "C", "R", 1, x);
				ImageIcon Piece = new ImageIcon("Grass.png");
				panel.add(Board2[x][y]);
			}
		}
		NewGame.setBackground(DarkSand);
		NewGame.setForeground(Color.WHITE);
		NewGame.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		NewGame.setText("NEW GAME");
		NewGame.setSize(236, 61);
		NewGame.setLocation(10, 89);
		NewGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				javax.swing.SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						ButtonsWork();
						// FrameCyvasse.NewGame(Board2, NextPieceIMG);
					}
				});
			}
		});
		InfoPane.setSize(256, 508);
		InfoPane.setLocation(528, 11);
		InfoPane.setBackground(Sand);
		panel.setLocation(10, 11);
		panel2.setBackground(Color.BLACK);
		panel2.setSize(800, 522);
		panel.setSize(508, 508);
		panel2.setLayout(null);
		panel2.add(InfoPane);
		InfoPane.setLayout(null);
		NextPiece.setFont(new Font("Segoe UI Light", Font.PLAIN, 15));
		NextPiece.setAlignmentX(Component.CENTER_ALIGNMENT);
		NextPiece.setSize(117, 28);
		NextPiece.setLocation(22, 260);
		
		InfoPane.add(NextPiece);
		NextPieceIMG.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		NextPieceIMG.setIconTextGap(0);
		NextPieceIMG.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		NextPieceIMG.setBackground(DarkSand);
		NextPieceIMG.setLocation(149, 243);
		NextPieceIMG.setSize(68, 68);
		InfoPane.add(NextPieceIMG);
		NewGame.setFont(new Font("Segoe UI Light", Font.PLAIN, 24));
		InfoPane.add(NewGame);

		JButton Exit1 = new JButton();
		Exit1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		Exit1.setText("EXIT");
		Exit1.setForeground(Color.WHITE);
		Exit1.setFont(new Font("Segoe UI Light", Font.PLAIN, 24));
		Exit1.setBackground(new Color(82, 77, 59));
		Exit1.setBounds(10, 391, 236, 61);
		Exit1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		InfoPane.add(Exit1);

		P1vP2.setEnabled(false);
		P1vP2.setForeground(Color.WHITE);
		P1vP2.setFont(new Font("Segoe UI Light", Font.PLAIN, 11));
		P1vP2.setBounds(10, 161, 71, 71);
		P1vP2.setBackground(DarkSand);
		InfoPane.add(P1vP2);

		P1vAI.setEnabled(true);
		P1vAI.setForeground(Color.WHITE);
		P1vAI.setFont(new Font("Segoe UI Light", Font.PLAIN, 11));
		P1vAI.setBounds(94, 161, 71, 71);
		P1vAI.setBackground(DarkSand);
		InfoPane.add(P1vAI);
		P1vAI.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Tunes.Tunes("C:/Users/Angus/workspace/Cyvasse/Tunes/Tune.wav");
			}
		});

		AIvAI.setEnabled(false);
		AIvAI.setForeground(Color.WHITE);
		AIvAI.setFont(new Font("Segoe UI Light", Font.PLAIN, 11));
		AIvAI.setBounds(175, 161, 71, 71);
		AIvAI.setBackground(DarkSand);
		InfoPane.add(AIvAI);

		lblChooseWhereTo.setVisible(false);
		lblChooseWhereTo.setForeground(new Color(128, 0, 0));
		lblChooseWhereTo.setFont(new Font("Segoe UI Light", Font.PLAIN, 16));
		lblChooseWhereTo.setHorizontalAlignment(SwingConstants.CENTER);
		lblChooseWhereTo.setBounds(10, 317, 236, 63);
		InfoPane.add(lblChooseWhereTo);

		JLabel lblCyvasse = new JLabel("CYVASSE");
		lblCyvasse.setForeground(new Color(128, 0, 0));
		lblCyvasse.setHorizontalAlignment(SwingConstants.CENTER);
		lblCyvasse.setFont(new Font("Game of Thrones", Font.PLAIN, 39));
		lblCyvasse.setBounds(10, 11, 234, 74);
		InfoPane.add(lblCyvasse);
		panel2.add(panel);
		getContentPane().add(panel2);
		setIconImage(img.getImage());
		setName(WindowName);
		setSize(800, 559);
	}

	public static void ButtonsWork() {
		AIvAI.setEnabled(true);
		P1vP2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FrameCyvasse.NewGame(Board2, NextPieceIMG, ("wr"));
				P1vAI.setEnabled(false);
				P1vP2.setEnabled(false);
				AIvAI.setEnabled(false);

			}
		});
		P1vAI.setEnabled(true);
		P1vP2.setEnabled(true);
	}
}
