package Game;

import java.awt.Color;
import java.awt.Font;
import java.awt.TextField;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

@SuppressWarnings("unused")
public class Frame extends JFrame{
	public static void main(String[]args) {
		JFrame frame = new JFrame("Cyvasse");//
		frame.setSize(1080, 720);//
		int Step=1;//
		String[][] board = new String[8][8];//
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//
		JLayeredPane panel = new JLayeredPane();//
		String text = "";
		TextField Console = new TextField();
		JTextArea ConsoleHistory = new JTextArea();
		JTextPane Board = new JTextPane();
		Board.setContentType("text/html");
		Board.setBounds(5,5,630,630);
		Board.setBackground(Color.BLACK);
		Board.setEditable(false);
		Console.setBounds(640, 550, 400, 25);
		ConsoleHistory.setEditable(false);
		ConsoleHistory.setText(text);
		JScrollPane scroll = new JScrollPane (ConsoleHistory, 
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		Font font = new Font("Segoe UI", Font.PLAIN, 15);
		scroll.setBounds(640, 5,400,545);
		Console.setFont(font);
		Console.setBackground(Color.white);
		Console.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					String outcome = Console.getText();
					ConsoleHistory.append(outcome+" \n");
					System.out.print(outcome);


				}
			}
		});
		//ConsoleHistory.setText(outcome);
		frame.add(panel);
		panel.add(Board, 3);
		panel.add(scroll, 2);
		panel.add(Console, 1);
		//
		frame.setVisible(true);
		String BoardText=("<font face = 'game of thrones' font size = '5' font color='red'>Cyvasse</font><br><br><font face = 'courier new' font size = '3' font color='white'>What would you like to do?<br><br>1)New Game<br>2)Settings<br>3)Quit");
		Board.setText("	<html>"+BoardText+"</html>");
		
	}
	

}
