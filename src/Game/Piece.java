package Game;

public class Piece{
	public Piece (int mv, int atk, String fn,  int NumPieces, boolean player){
		this.MoveDistance = mv;
		this.AttackDistance=atk;
		this.FileName=fn;
		this.NumOfPieces=NumPieces;
		this.Player=player;
	}
	int MoveDistance;
	int AttackDistance;
	String FileName;
	int NumOfPieces;
	Boolean Player;
}
/*All of the Piece values
 * 0=Mountain
 * 1=Rabble
 * 2=Spearman
 * 3=Crossbowman
 * 4=LightHorse
 * 5=HeavyHorse
 * 6=Elephant
 * 7=Trebuchet
 * 8=Catapult
 * 9=Dragon
 * 10=King
 */
