package Game;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;

@SuppressWarnings("serial")
public class Buttons extends JButton {

	int Value;
	static String Piece;
	int Tile;
	boolean Play;
	Piece P;
	int Player;
	int Moves;
	Boolean moveHere;
	Boolean enabled=true;
	int X;
	int Y;
	Color Normal;
	
	public void SetIcon(ImageIcon Icon) {
		setIcon(Icon);
	}

	public Buttons(String PieceName, int TileType, String Name, String Colour, int Num, int i) {

		Color Sand1 = new Color(158, 146, 99);
		Color Sand2 = new Color(82, 77, 59);

		Value = i;

		Tile = TileType;
		// ImageIcon Piece = new ImageIcon(PieceName + TileType + ".png");
		setSize(64, 64);
		if (TileType == 1) {
			Normal=Sand1;
			setBackground(Sand1);
		} else {
			setBackground(Sand2);
			Normal=Sand2;
		}
		// setIcon(Piece);

		addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
			}
		});

	}

}
